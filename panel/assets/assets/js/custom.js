$(document).ready(function () {
    $(".sortable").sortable();

    $(".removeBtn").on('click', function (e) {

        $data_url = $(this).data("url");
        Swal.fire({
            title: 'Silmek İstediğinize Emin Misiniz?',
            text: "Bu Ürün Siliniyor..",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sil',
            cancelButtonText: 'İptal',
        }).then((result) => {
            if (result.value) {
                window.location.href = $data_url;
            }
        });
    });

    $(".isActive").on('change', function () {
        var $data = $(this).prop('checked');
        var $data_url = $(this).data("url");

        if (typeof $data !== "undefined" && typeof $data_url !== "undefined") {
            $.post($data_url, {
                data: $data
            }, function (response) {
                // alert(response);
                Swal.fire({
                    icon: 'success',
                    title: response,
                    showConfirmButton: false,
                    timer: 1500
                })
            });
        }
    });

    $(".sortable").on('sortupdate', function (event, ui) {
        var $data = $(this).sortable("serialize");
        var $data_url = $(this).data("url");

        $.post($data_url, {
            data: $data
        }, function (response) {});
    });

    var upload_section = Dropzone.forElement("#dropzone");

    upload_section.on("complete", function () {
        // alert("yüklendi");
    });
});