<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            Ürün Güncelleme 
        </h4>
    </div><!-- END column -->

    <?php if(isset($form_error)): ?>
    <div class="col-md-12">
        <div class="alert alert-info alert-dismissible">
            <button class="close" data-dismiss="alert">&times;</button>
            <small class="text-primary" style="font-size:14px;"><?=$form_error?></small>
        </div>
    </div>
    <?php endif; ?>

    <?php if(isset($update)): ?>
    <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
            <button class="close" data-dismiss="alert">&times;</button>
            <small style="font-size:14px;">Ürün Başarıyla Eklendi!</small>
        </div>
    </div>
    <?php endif; ?>

    <div class="col-md-12">
        <div class="widget p-lg">
            <?php if(!empty($product_data)): ?>
            <form action="<?=base_url("product/update/".$product_data->id)?>" method="post">
                <div class="alert alert-primary text-primary" style="background-color:#454545;">
                    <h3><?=$product_data->title?></h3> Adlı ürün için düzenleme formu
                </div>

                <div class="form-group">
                    <label>Başlık</label>
                    <input type="text" class="form-control" value="<?=$product_data->title?>" name="title">
                </div>
                <div class="form-group">
                    <label>Açıklama</label>
                    <textarea name="description" data-plugin="summernote" data-options="{height:250}">
                        <?=$product_data->description?>
                    </textarea>
                </div>
                <button type="submit" class="btn btn-primary">Güncelle</button>
                <a href="<?=base_url("product")?>" type="submit" class="btn btn-danger">İptal</a>
            </form>
            <?php else: ?>
            <div class="alert alert-warning"><strong>Böyle bir ürün yok</strong></div>
            <?php endif;?>
        </div><!-- .widget -->
    </div><!-- END column -->
</div>