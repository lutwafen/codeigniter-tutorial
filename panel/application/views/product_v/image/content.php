<div class="row">
    <div class="col-md-12">
        <h4 class="m-b-lg">
            <?=$item->title?> Adlı Ürünün Fotoğrafları
        </h4>
    </div><!-- END column -->

    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <form action="<?=base_url("product/image_upload/".$item->id)?>" class="dropzone" id="dropzone" data-plugin="dropzone" data-options="{ url: '<?=base_url("product/image_upload/".$item->id)?>'}">
                    <div class="dz-message">
                        <h3 class="m-h-lg">Yüklemek istediğiniz resimleri buraya sürükleyebilirsiniz veya tıklayarak seçebilirsiniz.</h3>
                        <p class="m-b-lg text-muted">(Dosyanın lütfen resim formatında olduğuna dikkat edin!)</p>
                    </div>
                </form>
            </div>
        </div><!-- .widget-body -->
    </div><!-- .widget -->
</div>

<div class="row">
    <div class="col-md-12">
        <div class="widget">
            <div class="widget-body">
                <?php if(!empty($item_images)): ?>
                <table class="table table-bordered table-stripped table-hover pictures_list">
                    <thead>
                        <th>#id</th>
                        <th>Görsel</th>
                        <th>Resim Adı</th>
                        <th>Durumu</th>
                        <th>İşlem</th>
                    </thead>
                    <tbody>
                        <?php foreach($item_images as $image): ?>
                        <tr>
                            <td class="w100 text-center">#<?=$image->id?></td>
                            <td class="w100">
                                <img
                                width="75"
                                src="<?=base_url("uploads/{$viewFolder}/")?><?=$image->img_url?>"
                                alt="" class="img-responsive">
                            </td>
                            <td><?=$image->img_url?></td>
                             <td class="w100 text-center">
                                <input 
                                class="isActive" 
                                <?=$image->isActive == "1" ? "checked" : ""?>
                                id="switch-2-2" type="checkbox" data-switchery data-color="#10c469"  />
                            </td>
                            <td class="w100 text-center">
                                <a class="btn btn-sm btn-info btn-outline btn-block"><i class="fa fa-pencil-square-o"></i> Düzenle</a>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
                <?php endif; ?>
            </div>
        </div><!-- .widget-body -->
    </div><!-- .widget -->
</div>

