<?php

class Product extends CI_Controller
{
    public $viewFolder = "";

    public function __construct()
    {
        parent::__construct();
        $this->viewFolder = "product_v";
        $this->load->model("Product_model", "product");
        $this->load->model("Product_image_model", "product_image");
    }

    public function index(){

        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "list";
        $viewData->items = $this->product->get_all(array(), "rank ASC");

        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    function new_form()
    {
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    function save()
    {
        $this->load->library("form_validation");

        $this->form_validation->set_rules("title","Başlık","required|trim");
        $this->form_validation->set_message(
            array(
                "required" => "<strong>{field}</strong> Alanını Girmelisiniz" // field -> tag
            )
        );
        $validate = $this->form_validation->run(); // return true or false
                
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "add";
        if($validate)
        {
            $insert = $this->product->add(array(
                "title" => $this->input->post('title'),
                "description" => $this->input->post("description"),
                "url" => convertToSEO($this->input->post('title')),
                "isActive" => 1,
                "createdAt" => date("Y-m-d H:i:s")
            ));
            
            //echo "kayıt başarılı";
            
            // TODO : Alert Sistemi Eklenecek
            redirect(base_url("product"));
        }
        else{
            //echo "Bir şeyler ters gitti";
            //echo validation_errors();

            $viewData->form_error = validation_errors();
        }
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        //echo "save";
        
    }

    function update_form($product_id)
    {
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        $viewData->product_data = $this->product->get(array("id" => $product_id));
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    }

    function update($product_id)
    {
        $this->load->library("form_validation");

        $this->form_validation->set_rules("title","Başlık","required|trim");
        $this->form_validation->set_message(
            array(
                "required" => "<strong>{field}</strong> Alanını Girmelisiniz" // field -> tag
            )
        );
        $validate = $this->form_validation->run(); // return true or false
                
        $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "update";
        if($validate)
        {
            $update = $this->product->update(
                array(
                    "title" => $this->input->post('title'),
                    "description" => $this->input->post("description"),
                    "url" => convertToSEO($this->input->post('title'))
                ), 
                array(
                    "id" => $product_id
                )
            );
            
            //echo "kayıt başarılı";
            
            // TODO : Alert Sistemi Eklenecek
            redirect(base_url("product"));
        }
        else{
            //echo "Bir şeyler ters gitti";
            //echo validation_errors();

            $viewData->form_error = validation_errors();
        }
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
        //echo "save";
        
    }

    public function delete($product_id)
    {
        $delete = $this->product->delete(array("id"=>$product_id));
        redirect(base_url("product"));
    }

    public function isActiveSetter($product_id)
    {
        if($product_id)
        {
            $isActive = ($this->input->post('data') === "true") ? 1 : 0;
            $update = $this->product->update(array("isActive" => $isActive), array("id" => $product_id));

            if($update)
                echo "Başarıyla Güncelleştirildi!";
            else
                echo "Bir Sorun Var";
        }
    }

    public function rankSetter()
    {
        $data = $this->input->post("data");
        // print_r($data);
        parse_str($data, $order);

        $items = $order['ord'];
        
        foreach($items as $rank => $id)
        {
            $this->product->update(
                array(
                    "rank" => $rank
                ),
                array(
                    "rank != " => $rank,
                    "id" => $id
                )
            );
        }
    }
    
    public function image_form($product_id)
    {
         $viewData = new stdClass();
        $viewData->viewFolder = $this->viewFolder;
        $viewData->subViewFolder = "image";

        $viewData->item = $this->product->get(array("id" => $product_id));
        $viewData->item_images = $this->product_image->get_all(array("product_id" => $product_id));
        $this->load->view("{$viewData->viewFolder}/{$viewData->subViewFolder}/index", $viewData);
    
    }

    public function image_upload($id)
    {
        $file_name = convertToSeo(pathinfo($_FILES['file']['name'], PATHINFO_FILENAME));
        $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

        $file = $file_name.'.'.$ext;

        $config =array(
            "allowed_types" => "jpg|jpeg|png|gif",
            "upload_path" => "uploads/{$this->viewFolder}",
            "file_name" => $file
        );

        
        $this->load->library("upload", $config);     
        $upload = $this->upload->do_upload("file");

        if($upload)
        {
            $uploded_files = $this->upload->data("file_name");
            $this->product_image->add(
                array(
                    "img_url" => $file,
                    "rank" => 0,
                    "isActive" => 1,
                    "isCover" => 0,
                    "createdAt" => date("Y-m-d H:i:s"),
                    "product_id" => $id
                )
            );
        }

    }
}
