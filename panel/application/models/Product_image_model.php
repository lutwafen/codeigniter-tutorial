<?php 
    class Product_Image_model extends CI_Model
    {
        public $tableName = "product_images";
        function __construct()
        {
            parent::__construct();
        }

        public function get_all($where = array(), $order = "id ASC")
        {
            return $this->db->where($where)->order_by($order)->get($this->tableName)->result();
        }
        function get($where)
        {
            return $this->db->where($where)->get($this->tableName)->row();
        }
        public function add($data = array())
        {
            return $this->db->insert($this->tableName, $data);
        }
        public function update($data  = array(), $where = array())
        {
            return $this->db->where($where)->update($this->tableName, $data);
        }
        public function delete($where = array())
        {
            return $this->db->where($where)->delete($this->tableName);
        }
    }